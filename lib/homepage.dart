import 'menu.dart';
import 'package:flutter/material.dart';
import 'music.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  final Container _leftContainer = Container(
    child: const Align(
        alignment: Alignment.centerRight,
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Icon(Icons.delete, color: Colors.white),
        )),
    color: Colors.red,
  );
  final Container _rightContainer = Container(
    child: const Align(
        alignment: Alignment.centerLeft,
        child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Icon(Icons.favorite, color: Colors.white))),
    color: Colors.green,
  );
  final snackBarYes = const SnackBar(
    content: Text(
      'Ajouté aux favoris', 
      style: TextStyle(color: Colors.green)
      )
    );
  final snackBarNo = const SnackBar(
    content: Text(
      'Retirer des favoris', 
      style: TextStyle(color: Colors.red)
      )
    );
  List<Music> _musics = getList();
  
  @override
  Widget build(BuildContext context) {
    _musics = rearrangeSound(_musics);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ynovify',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
                color: Colors.white)),
      ),
      body: ListView.builder(
          itemCount: _musics.length,
          padding: const EdgeInsets.symmetric(vertical: 16),
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
                background: _rightContainer,
                secondaryBackground: _leftContainer,
                key: ValueKey<int>(index),
                child: ListTile(
                  title: Text(
                    _musics[index].toString(),
                    style: TextStyle(
                      color: ( _musics[index].isFavorite ? Colors.red : Colors.black)
                    ),
                  ),
                ),
                onDismissed: (DismissDirection direction) {
                  _musics.removeAt(index);
                },
                confirmDismiss: (DismissDirection direction) async {
                  if (direction == DismissDirection.endToStart) {
                    return await showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text('Voulez vous supprimer le truc'),
                          content: const Text(
                              "Are you sure you want to delete this item?"),
                          actions: <Widget>[
                            TextButton(
                              child: const Text('Confirmer'),
                              onPressed: () {
                                Navigator.of(context).pop(true);
                              },
                            ),
                            TextButton(
                              child: const Text('Annuler'),
                              onPressed: () {
                                Navigator.of(context).pop(false);
                              },
                            )
                          ],
                        );
                      },
                    );
                  }else{
                    setState(() {
                      bool state = toggleFavorite(_musics[index]);
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      ScaffoldMessenger.of(context).showSnackBar((state ? snackBarYes:snackBarNo));
                    });
                  }
                });
          }),
      bottomNavigationBar: myMenu(context),
    );
  }
}
