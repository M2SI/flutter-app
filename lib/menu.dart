import 'package:flutter/material.dart';

BottomNavigationBar myMenu(BuildContext context){

  // ignore: dead_code
  return BottomNavigationBar(
    unselectedItemColor: Colors.white,
    selectedItemColor: Colors.white,
    backgroundColor: Colors.black,
    items: const <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.list, color: Colors.white),
        label: 'Musiques',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.headphones, color: Colors.white),
        label: 'En cours',

      )
    ],
    onTap: (int index) {
      if (index == 1) {
        Navigator.pushNamed(context, '/listen');
      }else{
        Navigator.pushNamed(context, '/');
      }
    }
  ); 
}



