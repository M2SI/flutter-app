import 'package:flutter/material.dart';
import 'listenpage.dart';
import 'homepage.dart';

void main() {
  runApp(const MyApp());
}


class MyApp extends StatelessWidget {

  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => const MyHomePage(title: 'Ynovifi'),
        '/listen': (context) => const ListenPage(title: 'Ynovifi',),
      },
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.black,
          centerTitle: true
        )
      ),

      // home: const MyHomePage(title: 'YnovIfy'),
      // home: (_variable == 'home' ? MyHomePage(title: 'ynovify') : OtherPage),

    );
  }
}




