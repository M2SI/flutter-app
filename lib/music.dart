
class Music {
  int id;
  String name;
  String picture;
  String soundLink;
  String author;
  bool isFavorite;

  Music(this.id, this.name, this.soundLink, this.picture, this.author, this.isFavorite);

  @override
  String toString() {
    return '#' + id.toString() + ' - '+ name + ' | ' + author;
  }
  
}

List<Music> getList(){
  return [
    Music(1, 'la kokaracha', 'monilen', 'monimage', 'Geronimo', false),
    Music(2, 'la kokaracha', 'monilen', 'monimage', 'Geronimo', false),
    Music(3, 'la kokaracha', 'monilen', 'monimage', 'Geronimo', true),
    Music(4, 'la kokaracha', 'monilen', 'monimage', 'Geronimo', false),
    Music(5, 'la kokaracha', 'monilen', 'monimage', 'Geronimo', true)
  ];
}

bool toggleFavorite(Music sound){
  return sound.isFavorite = !sound.isFavorite;
}

List<Music> rearrangeSound(List<Music> sounds){
  print(sounds.length);
  List<Music> _favorite = [];
  List<Music> _nonFavorite = [];
  for(Music sound in sounds){
    if(sound.isFavorite){
      _favorite.add(sound);
    }else{
      _nonFavorite.add(sound);
    }
  }
  print([..._favorite, ..._nonFavorite].length);
  return [..._favorite, ..._nonFavorite];
}