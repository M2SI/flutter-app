import 'package:flutter/material.dart';
import 'menu.dart';

class ListenPage extends StatefulWidget {
  const ListenPage({Key? key, required this.title}) : super(key: key);
  final String title;
  
  @override
  State<ListenPage> createState() => _ListenPageState();
}

class _ListenPageState extends State<ListenPage> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const Text('data'),
      bottomNavigationBar: myMenu(context),
    );
  }

}